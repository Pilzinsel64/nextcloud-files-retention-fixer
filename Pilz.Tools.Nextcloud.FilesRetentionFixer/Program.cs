﻿using Mono.Options;
using Pilz.Networking.CloudProviders.Nextcloud;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Apps.FileRetention.Model;
using Pilz.Networking.CloudProviders.Nextcloud.Client.Cloud.Model;

bool shouldShowHelp = false;
NextcloudLogin creds = new();

// Try parsing the command line arugmends

OptionSet optionSet = new()
{
    {
        "s|server=",
        "The {SERVER} url of the nextcloud instance.",
        loginName => creds.Server = loginName
    },
    {
        "l|login=",
        "The {LOGIN} name to use.\nNote: The user must be an Admin!",
        loginName => creds.LoginName = loginName
    },
    {
        "p|password=",
        "The (app){PASSWORD} for the user.",
        appPassword => creds.AppPassword = appPassword
    },
    {
        "h|help",
        "Shows this help message.",
        help => shouldShowHelp = true
    },
};

try
{
    optionSet.Parse(args);
}
catch (OptionException ex)
{
    Console.Write("Argument Error: ");
    Console.WriteLine(ex.Message);
    Console.WriteLine("Try '--help' for more information.");
    return;
}

// Draw help message

if (shouldShowHelp)
{
    optionSet.WriteOptionDescriptions(Console.Out);
    return;
}

// Login into Nextcloud

using NextcloudClient ncClient = new();
UserInfo? userInfo = ncClient.Login(creds);

if (userInfo is null)
{
    Console.WriteLine("The current user credentials are probably wrong or the user is disabled.");
    return;
}

// Check all existing file retention rules

FilesRetentionClient retentionClient = ncClient.GetClient<FilesRetentionClient>();
RetentionRule[]? rules = retentionClient.GetRetentionRules();

if (rules is not null)
{
    foreach (var rule in rules)
    {
        if (!rule.HasJob && retentionClient.DeleteRetentionRule(rule.ID))
        {
            if (retentionClient.CreateRetentionRule(rule))
                Console.WriteLine($"Re-created job for TagId={rule.TagID} successfully.");
            else
                Console.WriteLine($"ERROR: Id={rule.ID} TagId={rule.TagID} TimeUnit={rule.TimeUnit} TimeAfter={rule.TimeAfter} TimeAmount={rule.TimeAmount}");
        }
    }
}

Console.WriteLine("FINISHED");
